﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    const float popTime = 2f;
    float timePassed;

    GameObject ballPrefab;
    bool isFull;

    // Start is called before the first frame update
    void Start()
    {
        timePassed = 0f;
        isFull = false;

        ballPrefab = Resources.Load<GameObject>("Ball");
    }

    public void GenerateNewBall()
    {
        isFull = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!isFull)
        {
            return;
        }

        timePassed += Time.deltaTime;

        if(timePassed >= timePassed)
        {
            isFull = true;

            CreateBall();
        }
    }

    void CreateBall()
    {
        GameObject.Instantiate(ballPrefab, this.transform.position, this.transform.rotation);
    }
}
