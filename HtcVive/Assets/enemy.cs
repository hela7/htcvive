﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    const float popTimer = 3f;
    float timePassed;
    bool isDead;

    void Start()
    {
        timePassed = 0f;
        isDead = false;
    }

    void Update()
    {
        if(!isDead)
        {
            return;
        }

        timePassed += Time.deltaTime;

        if(timePassed >= popTimer)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(this.gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        isDead = true;
    }
}
